import paho.mqtt.client as mqtt
import json
import time
import os
import socket

import argparse
import yaml

if __name__ == '__main__':
  parser = argparse.ArgumentParser(prog='logbucket')
  parser.add_argument('-c', '--config', type=str, required=True,
                      help='path to configuration file (YAML)')
  args = parser.parse_args()
  with open(args.config, 'r') as config_file:
    cfg = yaml.load(config_file)
  pid = os.getpid()
  client = mqtt.Client('app-log-client-01')
  client.connect(cfg['mqtt']['host'])
  for i in range(0, 30):
    print(i)
    topic = 'logbucket/app-log/{}/{}/testapp/info'.format(socket.gethostname(),
                                                          os.getpid())
    client.publish(topic, json.dumps({
                    'level': 'info',
                    'file': __file__,
                    'function': 'main',
                    'description': 'loop counter incremented',
                    'value': i,
                  }))
    time.sleep(1)
