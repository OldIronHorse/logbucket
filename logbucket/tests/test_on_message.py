from unittest import TestCase
from unittest import mock
from unittest.mock import MagicMock
from datetime import datetime
from logbucket.logbucket import on_message, all_payload_handlers

class TestOnMessage(TestCase):
  def setUp(self):
    self.db = {
      '_domain_s': MagicMock(),
      '_domain_j': MagicMock(),
      '_domain_i': MagicMock()
    }
    self.handlers = {
      '_domain_i': all_payload_handlers['integer'],
      '_domain_j': all_payload_handlers['json'],
      '_domain_s': all_payload_handlers['string']
    }
  
  @mock.patch('logbucket.logbucket.datetime')
  def test_string_message(self, dt):
    dt.now.return_value = datetime(1969,9,17,10,45)
    msg = MagicMock()
    msg.topic = '_root_/_domain_s'
    msg.payload = MagicMock()
    msg.payload.decode = MagicMock(return_value="the message string")

    on_message(None, {'db': self.db, 'handlers_by_domain': self.handlers}, msg)

    msg.payload.decode.assert_called_once_with('utf-8')
    self.db['_domain_s'].insert_one.assert_called_once_with({
      'payload': 'the message string',
      'topic': ['_root_', '_domain_s'],
      'timestamp': '1969-09-17T10:45:00.000000 '
    })

  @mock.patch('logbucket.logbucket.datetime')
  def test_int_message(self, dt):
    dt.now.return_value = datetime(1969,9,17,10,45)
    msg = MagicMock()
    msg.topic = '_root_/_domain_i'
    msg.payload = MagicMock()
    msg.payload.decode = MagicMock(return_value="1234")

    on_message(None, {'db': self.db, 'handlers_by_domain': self.handlers}, msg)

    msg.payload.decode.assert_called_once_with('utf-8')
    self.db['_domain_i'].insert_one.assert_called_once_with({
      'payload': 1234,
      'topic': ['_root_', '_domain_i'],
      'timestamp': '1969-09-17T10:45:00.000000 '
    })

  @mock.patch('logbucket.logbucket.datetime')
  def test_json_message(self, dt):
    dt.now.return_value = datetime(1969,9,17,10,45)
    msg = MagicMock()
    msg.topic = '_root_/_domain_j'
    msg.payload = MagicMock()
    msg.payload.decode = MagicMock(return_value='''
    {
      "s1" : "a string", 
      "s2" : "another string",
      "i1" : 1,
      "i2" : 2,
      "f1" : 1.5,
      "f2" : 2.0
    } ''')

    on_message(None, {'db': self.db, 'handlers_by_domain': self.handlers}, msg)

    msg.payload.decode.assert_called_once_with('utf-8')
    self.db['_domain_j'].insert_one.assert_called_once_with({
      'payload': {
        's1': 'a string',
        's2': 'another string',
        'i1': 1,
        'i2': 2,
        'f1': 1.5,
        'f2': 2.0
      },
      'topic': ['_root_', '_domain_j'],
      'timestamp': '1969-09-17T10:45:00.000000 '
    })
